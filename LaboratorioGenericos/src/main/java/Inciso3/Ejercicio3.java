/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Inciso3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hp
 */
public class Ejercicio3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Asignatura asignatura = null;
        AsignaturaModelImp aimpl = null;
        int opc = 0, i = 1;
        Scanner scan = new Scanner(System.in);
        do {
            try {
                menu();
                System.out.print("opc: ");
                opc = scan.nextInt();
                aimpl = new AsignaturaModelImp();
                switch (opc) {
                    case 1:
                        System.out.println("escriba el nombre de la clase");
                        String nombre = scan.next();
                        System.out.println("escriba nombre del maestro a dar la clase");
                        String docente = scan.next();
                        System.out.println("escriba la cantidad de creditos disponibles");
                        int creditos = scan.nextInt();
                        asignatura = new Asignatura(i++, nombre, docente, creditos);
                        aimpl.save(asignatura);
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        System.out.println("Digite nombre de la asignatura a buscar");
                        String nombreAsi = scan.next();
                        Asignatura asig = aimpl.buscarByNombre(nombreAsi);
                        if (asig != null) {
                            System.out.println(asig.toString());
                        } else {
                            System.out.println("No se encontro la asignatura");
                        }
                        break;
                    case 6:
                        System.out.println("Digite nombre del docente para buscar asignatura");
                        String nombreDoc = scan.next();
                        Asignatura doc = aimpl.buscarByNombre(nombreDoc);
                        if (nombreDoc != null) {
                            System.out.println(nombreDoc.toString());
                        } else {
                            System.out.println("No se encontro al docente de esa asignatura");
                        }
                        break;
                    case 7:
                        Asignatura[] asignaturas = aimpl.findAll();
                        if (asignatura != null) {
                            for (Asignatura a : asignaturas) {
                                System.out.println(a.toString());

                            }
                        }
                        break;
                    case 8:
                        System.exit(0);
                        break;
                    default:
                        System.out.println("Seleccion invalida");
                        break;
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Ejercicio3.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Ejercicio3.class.getName()).log(Level.SEVERE, null, ex);
            }
        } while (opc != 8);
    }

    public static void menu() {
        System.out.println("Menu de Opciones para el manejo de Asignaturas");
        System.out.println("1. Registrar Asigantura");
        System.out.println("2. Editar asignatura");
        System.out.println("3. Eliminar asignatura");
        System.out.println("5. Buscar por nombre");
        System.out.println("6. Buscar por docente");
        System.out.println("7. Listar todos las asignaturas");
        System.out.println("8. Salir");
    }
}

class Asignatura {

    private int id;
    private String nombre;
    private String docente;
    private int creditos;

    public Asignatura(int id, String nombre, String docente, int creditos) {
        this.id = id;
        this.nombre = nombre;
        this.docente = docente;
        this.creditos = creditos;
    }

    Asignatura() {

    }

    @Override
    public String toString() {
        return "Asignatura{" + "id=" + id + ", nombre=" + nombre + ", docente=" + docente + ", creditos=" + creditos + '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDocente() {
        return docente;
    }

    public void setDocente(String docente) {
        this.docente = docente;
    }

    public int getCreditos() {
        return creditos;
    }

    public void setCreditos(int creditos) {
        this.creditos = creditos;
    }

}

interface Model<T> {

    void save(T t) throws IOException;

    boolean update(T t) throws IOException;

    void delete(T t) throws IOException;

    T[] findAll() throws IOException;
}

interface AsignaturaModel extends Model<Asignatura> {

    Asignatura buscarByNombre(String nombre) throws IOException;

    Asignatura buscarByDocente(String docente) throws IOException;
}

class AsignaturaModelImp implements AsignaturaModel {

    private File file;
    private RandomAccessFile raf;
    private String path;
    private final int SIZE = 68;

    private void openRAF() throws IOException {
        if (!file.exists()) {
            file.createNewFile();
            raf = new RandomAccessFile(file, "rw");
            raf.seek(0);
            raf.writeInt(0);
            raf.writeInt(0);
        } else {
            raf = new RandomAccessFile(file, "rw");
        }
    }

    private void closeRAF() throws IOException {
        if (raf != null) {
            raf.close();
        }
    }

    public AsignaturaModelImp() {
        path = "base.dat";
        file = new File(path);
    }

    public AsignaturaModelImp(String path) {
        this.path = path;
        file = new File(path);
    }

    public AsignaturaModelImp(File file) {
        this.file = file;
    }

    @Override
    public Asignatura buscarByNombre(String nombre) throws IOException {
        Asignatura[] asignaturas = findAll();
        Asignatura asignatura = null;
        if (asignaturas != null) {
            for (Asignatura a : asignaturas) {
                if (a.getNombre().trim().equalsIgnoreCase(nombre)) {
                    return a;
                }
            }
        }
        return asignatura;
    }

    @Override
    public Asignatura buscarByDocente(String docente) throws IOException {
        Asignatura[] asignaturas = findAll();
        Asignatura asignatura = null;
        if (asignaturas != null) {
            for (Asignatura a : asignaturas) {
                if (a.getNombre().trim().equalsIgnoreCase(docente)) {
                    return a;
                }
            }
        }
        return asignatura;
    }

    @Override
    public void save(Asignatura t) throws IOException {
        openRAF();
        raf.seek(0);
        int n = raf.readInt();
        int k = raf.readInt();
        long pos = 8 + SIZE * n;
        raf.seek(pos);
        raf.writeInt(++k);
        raf.writeUTF(fixString(t.getNombre(), 20));
        raf.writeUTF(fixString(t.getDocente(), 10));
        raf.writeInt(t.getCreditos());
        raf.seek(0);
        raf.writeInt(++n);
        raf.writeInt(k);
        closeRAF();
    }

    private String fixString(String text, int capacity) {
        StringBuilder sb = null;
        if (text == null) {
            sb = new StringBuilder(capacity);
        } else {
            sb = new StringBuilder(text);
            sb.setLength(capacity);
        }
        return sb.toString();
    }

    @Override
    public void delete(Asignatura t) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Asignatura[] findAll() throws IOException {
        Asignatura[] asignaturas = null;
        openRAF();
        raf.seek(0);
        int n = raf.readInt();
        Asignatura e;
        for (int i = 0; i < n; i++) {
            long pos = 8 + i * SIZE;
            raf.seek(pos);
            e = new Asignatura();
            e.setId(raf.readInt());
            e.setNombre(raf.readUTF());
            e.setDocente(raf.readUTF());
            e.setCreditos(raf.readInt());
            asignaturas = addElement(asignaturas, e);
        }
        closeRAF();
        return asignaturas;
    }

    @Override
    public boolean update(Asignatura t) throws IOException {
        int id = t.getId();
        openRAF();
        raf.seek(0);
        int n = raf.readInt();
        if (id <= 0 || id > n) {
            closeRAF();
            return false;
        }
        long pos = 8 + (id - 1) * SIZE;
        raf.seek(pos);
        int code = raf.readInt();//el id no se cambia 
        if (code == id) {
            raf.writeUTF(fixString(t.getNombre(), 20));
            raf.writeUTF(fixString(t.getDocente(), 10));
            raf.writeInt(t.getCreditos());
            closeRAF();
            return true;
        }
        closeRAF();
        return false;
    }

    private Asignatura[] addElement(Asignatura asi[], Asignatura e) {
        if (asi == null) {
            asi = new Asignatura[1];
            asi[0] = e;
            return asi;
        }
        asi = Arrays.copyOf(asi, asi.length + 1);
        asi[asi.length - 1] = e;
        return asi;
    }
}
