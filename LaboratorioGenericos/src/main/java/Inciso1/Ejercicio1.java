/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Inciso1;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Hp
 */
public class Ejercicio1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List<String> a1 = new ArrayList<>();
        a1.add("Juan");
        a1.add("Pepito");
        a1.add("Perez");
        a1.add("Ana");
        Ordenar(a1);
        System.out.println(a1);
    }

    public static <T extends Comparable<T>> void Ordenar(List<T> namae){
        T aux;
        for (int i = 1; i < namae.size(); i++) {
            aux = namae.get(i);
            int j = i - 1;
            while ((j >= 0) && (aux.compareTo(namae.get(j))) < 0) {
                namae.remove(j + 1);
                namae.add(j + 1, namae.get(j));
                j--;
            }
            namae.remove(j + 1);
            namae.add(j + 1, aux);
        }
    }
}
