/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Inciso2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Hp
 */
public class Ejercicio2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Boolean bol[] = new Boolean[5];
        bol[0] = false;
        bol[1] = true;
        bol[2] = true;
        bol[3] = false;
        bol[4] = true;
        Ordenar(bol);

        for (Boolean b : bol) {
            System.out.println(b);
        }

    }

    public static <T extends Comparable<T>> void Ordenar(T[] name) {
        T aux;
        for (int i = 1; i < name.length; i++) {
            aux = name[i];
            int j = i - 1;
            while ((j >= 0) && (aux.compareTo(name[j])) < 0) {
                name[j + 1] = name[j];
                j--;
            }
            name[j + 1] = aux;
        }
    }

}
