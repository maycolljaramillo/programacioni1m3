/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculosejercicio2;

/**
 *
 * @author Sistemas36
 */
public class CalcularIR {

    public static float calculoIR(float salario) {
        float baseDis = salario - Otroscalculos.InssLaboral(salario);
        float salAnual = baseDis * 12;
        int caso = caso(salAnual);
        float deducible = deducible(caso);
        float SalMDed = salAnual - deducible;
        float porcentaje = porcentaje(caso);
        float porcentajeAplicado = SalMDed * porcentaje;
        float impuestoBase = impuestoBase(caso);
        float IRAnual = porcentajeAplicado + impuestoBase;
        float IrMensual = IRAnual / 12;
        return IrMensual;
    }

    private static int caso(float salario) {
        if (salario > 0 && salario <= 100000) {
            return 1;
        } else if (salario > 100000 && salario <= 200000) {
            return 2;
        } else if (salario > 200000 && salario <= 300000) {
            return 3;
        } else if (salario > 300000 && salario <= 500000) {
            return 4;
        } else {
            return 5;
        }
    }

    private static float deducible(int caso) {
        float deducible = 0;
        switch (caso) {
            case 1:
                deducible = 0;
                break;
            case 2:
                deducible = 100000;
                break;
            case 3:
                deducible = 200000;
                break;
            case 4:
                deducible = 350000;
                break;
            case 5:
                deducible = 500000;
                break;
        }
        return deducible;
    }

    private static float porcentaje(int caso) {
        float por = 0;
        switch (caso) {
            case 1:
                por = 0;
                break;
            case 2:
                por = (float) 0.15;
                break;
            case 3:
                por = (float) 0.2;
                break;
            case 4:
                por = (float) 0.25;
                break;
            case 5:
                por = (float) 0.3;
                break;
        }
        return por;
    }

    private static float impuestoBase(int caso) {
        float imBase = 0;
        switch (caso) {
            case 1:
                imBase = 0;
                break;
            case 2:
                imBase = 0;
                break;
            case 3:
                imBase = 15000;
                break;
            case 4:
                imBase = 45000;
                break;
            case 5:
                imBase = 82000;
                break;
        }
        return imBase;
    }
}
