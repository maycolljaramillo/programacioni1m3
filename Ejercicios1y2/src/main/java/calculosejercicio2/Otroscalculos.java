/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculosejercicio2;

/**
 *
 * @author Sistemas36
 */
public class Otroscalculos {

    public static float CalculoHorasExtras(int horas, float salario) {
        String s = String.valueOf(horas);
        float totalH = Float.parseFloat(s);
        return ((float) (salario / 240) * 2 * totalH);
    }

    public static float calculoAntiguedad(int antiguedad, float salario) {
        float aPagar = 0;
        switch (antiguedad) {
            case 0:
                aPagar = salario * 0;
                break;
            case 1:
                aPagar = (float) (salario * 0.03);
                break;
            case 2:
                aPagar = (float) (salario * 0.05);
                break;
            case 3:
                aPagar = (float) (salario * 0.07);
                break;
            case 4:
                aPagar = (float) (salario * 0.09);
                break;
            case 5:
                aPagar = (float) (salario * 0.1);
                break;
            case 6:
                aPagar = (float) (salario * 0.11);
                break;
            case 7:
                aPagar = (float) (salario * 0.12);
                break;
            case 8:
                aPagar = (float) (salario * 0.13);
                break;
            case 9:
                aPagar = (float) (salario * 0.14);
                break;
            case 10:
                aPagar = (float) (salario * 0.15);
                break;
            case 11:
                aPagar = (float) (salario * 0.155);
                break;
            case 12:
                aPagar = (float) (salario * 0.16);
                break;
            case 13:
                aPagar = (float) (salario * 0.165);
                break;
            case 14:
                aPagar = (float) (salario * 0.17);
                break;
            case 15:
                aPagar = (float) (salario * 0.175);
                break;
            case 16:
                aPagar = (float) (salario * 0.18);
                break;
            case 17:
                aPagar = (float) (salario * 0.185);
                break;
            case 18:
                aPagar = (float) (salario * 0.19);
                break;
            case 19:
                aPagar = (float) (salario * 0.195);
                break;
            case 20:
                aPagar = (float) (salario * 0.2);
                break;
            default:
                aPagar = (float) (salario * 0.22);
                break;
        }
        return aPagar;
    }

    public static float InssLaboral(float salario) {
        return (float) (salario * 0.07);
    }

    public static float inssPatronal(float salario) {
        return (float) (salario * 0.225);
    }

    public static float inatec(float salario) {
        return (float) (salario * 0.02);
    }

    public static float vacaciones(float salario) {
        return (float) (salario / 12);
    }

    public static float treceavomes(float salario) {
        return (float) (salario / 12);
    }
}
