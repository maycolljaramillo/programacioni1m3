/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import pojo.Empleado;
import pojo.Persona;

/**
 *
 * @author Sistemas36
 */
public class Application {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Persona p1 = new Persona();
        p1.setEdad(19);
        p1.setNombre("Juan");
        System.out.println("Edad: " + p1.getEdad());
        System.out.println("Nombre: " + p1.getNombre());
        System.out.println("");

        Persona p2 = new Empleado();
        p2.setEdad(29);
        p2.setNombre("Matias");
        System.out.println("Edad: " + p2.getEdad());
        System.out.println("Nombre: " + p2.getNombre());
        System.out.println("");

        Empleado e = new Empleado();
        e.setEdad(36);
        e.setNombre("Santiago");
        e.setSalarioBasico(4520);
        System.out.println("Edad: " + e.getEdad());
        System.out.println("Nombre: " + e.getNombre());
        System.out.println("Salaio: " + e.getSalarioBasico());
    }

}
