/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

/**
 *
 * @author Sistemas36
 */
public class Empleado extends Persona {

    private float salarioBasico;

    public Empleado() {
    }
    
    public Empleado(float salarioBasico, int edad, String nombre) {
        super(edad, nombre);
        this.salarioBasico = salarioBasico;
    }

    public float getSalarioBasico() {
        return salarioBasico;
    }

    public void setSalarioBasico(float salarioBasico) {
        this.salarioBasico = salarioBasico;
    }

    @Override
    public String toString() {
        return "Empleado{" + "salarioBasico=" + salarioBasico + '}';
    }
    
    public void NetoRecibir(){
        float ir = calculosejercicio2.CalcularIR.calculoIR(this.salarioBasico);
        float antiguedad= calculosejercicio2.Otroscalculos.calculoAntiguedadDiez(10, this.salarioBasico);
        float inssLab = calculosejercicio2.Otroscalculos.InssLaboral(this.salarioBasico);
        float inssPat=calculosejercicio2.Otroscalculos.inssPatronal(this.salarioBasico);
        float vacaciones = calculosejercicio2.Otroscalculos.vacaciones(this.salarioBasico);
        float treceavomes = calculosejercicio2.Otroscalculos.treceavomes(this.salarioBasico);
        float inatec= calculosejercicio2.Otroscalculos.inatec(this.salarioBasico);
        float neto = this.salarioBasico-inssLab-ir;
        System.out.println("El salario Neto A recibir del empleado es: "+neto);
        String text = String.format("IR=%8.2f Antiguedad=8.2f InssLab=8.2f InssPat=8.2f Vacaciones=8.2f Treceavomes=8.2f Inatec=8.2f", ir,antiguedad,
                inssLab,inssPat,vacaciones,treceavomes,inatec);
    }

}
