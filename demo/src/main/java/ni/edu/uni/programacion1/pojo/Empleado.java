/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.pojo;

import ni.edu.uni.programacion1.enums.NivelAcademico;
import ni.edu.uni.programacion1.enums.Sexo;

/**
 *
 * @author DocenteFCyS
 */
public class Empleado {

    private int codigo;
    private String cedula;
    private String primerNombre;
    private String segundoNombre;
    private String primerApellido;
    private String segundoApellido;
    private String direccion;
    private String telefono;
    private String celular;
    private Sexo sexo;
    private NivelAcademico nivelacademico;

    public Empleado() {
    }

    public Empleado(int codigo, String cedula, String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, String direccion, String telefono, String celular, Sexo sexo, NivelAcademico nivelacademico) {
        this.codigo = codigo;
        this.cedula = cedula;
        this.primerNombre = primerNombre;
        this.segundoNombre = segundoNombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.direccion = direccion;
        this.telefono = telefono;
        this.celular = celular;
        this.sexo = sexo;
        this.nivelacademico = nivelacademico;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public NivelAcademico getNivelacademico() {
        return nivelacademico;
    }

    public void setNivelacademico(NivelAcademico nivelacademico) {
        this.nivelacademico = nivelacademico;
    }

    @Override
    public String toString() {
        return "Empleado{" + 
                "codigo=" + codigo + 
                ", cedula=" + cedula + 
                ", primerNombre=" + primerNombre + 
                ", segundoNombre=" + segundoNombre + 
                ", primerApellido=" + primerApellido + 
                ", segundoApellido=" + segundoApellido + 
                ", direccion=" + direccion + 
                ", telefono=" + telefono + 
                ", celular=" + celular + 
                ", sexo=" + sexo + 
                ", nivelacademico=" + nivelacademico + 
                '}';
    }

    

}
