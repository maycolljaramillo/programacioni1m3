package pojo;

public class Cliente {
    
    private int id;
    private String nombre;
    private String apellido;
    private String nombreEmpresa;
    private String telefono;
    private String cedula;
    private String correo;
    private String direccion;
    private boolean activo;
    private String fechaCreacion;

    public Cliente() {
    }

    public Cliente(int id, String nombre, String apellido, String nombreEmpresa, String telefono, String cedula, String correo, String direccion, boolean activo, String fechaCreacion) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.nombreEmpresa = nombreEmpresa;
        this.telefono = telefono;
        this.cedula = cedula;
        this.correo = correo;
        this.direccion = direccion;
        this.activo = activo;
        this.fechaCreacion = fechaCreacion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Override
    public String toString() {
        return "Cliente{" + "id=" + id + ", nombre=" + nombre + ", apellido=" + apellido + ", nombreEmpresa=" + nombreEmpresa + ", telefono=" + telefono + ", cedula=" + cedula + ", correo=" + correo + ", direccion=" + direccion + ", activo=" + activo + ", fechaCreacion=" + fechaCreacion + '}';
    }
    
    
}
