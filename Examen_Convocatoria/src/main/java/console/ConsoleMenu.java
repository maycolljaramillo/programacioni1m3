package console;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import pojo.Cliente;
import pojo.Sucursal;


public class ConsoleMenu {
    
    public static void start() throws IOException{
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        
        int opcion,opMenu;
        boolean menu;
        
        Cliente c = null;
        Sucursal s = null;
        List clientes = null;
        List sucursales = null;
        
        
    do{    
        do{
        System.out.println("Escoja una opcion: \n"+
                "1. Registrar clientes y sucursales (COMPLETADO)\n"
              + "2. Listar clientes por cedula\n"
              + "3. Listar las sucursales por clientes\n"
                + "4. Listar clientes activos (COMPLETADO)\n"
                + "5. Listar todas las sucursales (COMPLETADO)\n"
                + "6. Listar clientes por rango fecha\n"
                + "7. Salir\nOpcion: ");
        opcion = Integer.parseInt(buffer.readLine());
        
        switch(opcion){
            case 1: clientes = ConsoleReader.readCliente(buffer); 
                    sucursales = ConsoleReader.readSucursal(buffer, clientes); 
                    System.out.println("\nCLIENTE Y SUCURSAL REGISTRADO EXITOSAMENTE!!");
                    ConsoleReader.printAllClientes(clientes);
                    ConsoleReader.printAllSucursales(sucursales); break;
            case 2: break;
            case 3: break;
                    
            case 4: ConsoleReader.printClientesActivos(clientes); 
                    System.out.println("ESTOS SON LOS CLIENTES ACTIVOS");break;
            case 5: ConsoleReader.printAllSucursales(sucursales);
                    System.out.println("ESTAS SON TODAS LAS SUCURSALES");break;
            case 6: break;
            case 7: System.exit(0);
        }
        }while((opcion<1) || (opcion>3));
        
        System.out.println("\n¿Desea continuar?: \n"+"1. Continuar    2. Salir");
        opMenu = Integer.parseInt(buffer.readLine());
        if(opMenu == 1){
            menu = true;
        }
        else{
            menu = false;
            break;
        }
    }while(menu = true);   
        
    }
}
