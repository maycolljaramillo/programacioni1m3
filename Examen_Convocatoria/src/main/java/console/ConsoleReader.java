package console;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import pojo.Cliente;
import pojo.Sucursal;

public class ConsoleReader {
    
    public static List readCliente(BufferedReader buffer) throws IOException{
        int id = 0;
        String nombre,apellido,nombreEmpresa,telefono,cedula,correo,direccion,fechaCreacion,
                validaActivo;
        boolean activo;
        
        List clientes = new ArrayList<>();
        
        System.out.println("DIGITE LOS DATOS A CONTINUACION: ");
        System.out.print("Nombres: ");
        nombre = buffer.readLine();
        System.out.print("Apellidos: ");
        apellido = buffer.readLine();
        System.out.print("Nombre Empresa: ");
        nombreEmpresa = buffer.readLine();
        System.out.print("Telefono: ");
        telefono = buffer.readLine();
        System.out.print("Cedula: ");
        cedula = buffer.readLine();
        System.out.print("Correo: ");
        correo = buffer.readLine();
        System.out.print("Direccion: ");
        direccion = buffer.readLine();
        System.out.print("Es cliente activo? Presione: 1.Si  2.No\t");
        validaActivo = buffer.readLine();
        if(validaActivo.equals("1")){
            activo = true;
        }else{
            activo = false;
        }
        System.out.print("Fecha Creacion: ");
        fechaCreacion = buffer.readLine();
        
        ++id;
        clientes.add(new Cliente(id,nombre,apellido,nombreEmpresa,telefono,cedula,correo,direccion,activo,fechaCreacion));
        return clientes;
    }
    
    public static List readSucursal(BufferedReader buffer,List clientes) throws IOException{
        int id = 0;
        String nombreSucursal,responsable,direccion,telefono;
        
        List sucursales = new ArrayList<>();
        
        System.out.println("DIGITE LOS DATOS DE LA SUCURSAL: ");
        System.out.print("Nombre Sucursal: ");
        nombreSucursal = buffer.readLine();
        System.out.print("Responsable: ");
        responsable = buffer.readLine();
        System.out.print("Direccion: ");
        direccion = buffer.readLine();
        System.out.print("Telefono: ");
        telefono = buffer.readLine();
        
        ++id;
        Cliente c = (Cliente) clientes.get((id-1));
        
        sucursales.add(new Sucursal(id,c,nombreSucursal,responsable,direccion,telefono));
        return sucursales;
    }
    
    public static void printAllClientes(List<Cliente> clientes){
        Iterator<Cliente> iterador = clientes.iterator();
        
        while(iterador.hasNext()){
            System.out.println(iterador.next());
        }
    }
    
    public static void printAllSucursales(List sucursales){
        Iterator iterador = sucursales.iterator();
        
        while(iterador.hasNext()){
            System.out.println(iterador.next());
        }
    }
    
    public static void printClientesActivos(List<Cliente> clientes){
        
        for(int i=0;i<clientes.size();i++){
            Cliente e = clientes.get(i);
            if(e.isActivo()){
                System.out.println(e);
            }
        }
    }
}
