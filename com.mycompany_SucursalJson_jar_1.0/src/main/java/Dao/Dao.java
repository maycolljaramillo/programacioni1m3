/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import java.io.IOException;

/**
 *
 * @author Sistemas36
 */
public interface Dao<T> {

     T[] findAll() throws IOException;
}
