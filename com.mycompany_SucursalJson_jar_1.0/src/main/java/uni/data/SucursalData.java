/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uni.data;
import com.google.gson.Gson;
import java.io.FileNotFoundException;
import java.io.FileReader;
import uni.pojo.Sucursal;

/**
 *
 * @author Sistemas36
 */
public class SucursalData {
    private Sucursal[] Sucursales;
    
    public SucursalData() throws FileNotFoundException{
     populateSucursal();
    }
    private void populateSucursal() throws FileNotFoundException{
     Gson gson = new Gson();
     Sucursales = gson.fromJson(new FileReader("MOCK_DATA.json"), Sucursal[].class);
  }
      public Sucursal[] getSucursales() {
        return Sucursales;
    }

   
 
}
